# Builder Source Generator Gradle Plugin

Gradle plugin that creates special folder for generated classes by Builder Source Generator Library and marks this
folder as a source set.

# Installation
* **Gradle 2.1 or newer**

    ```
    plugins {
        id "com.gitlab.tguseynov.builder-source-generator-gradle-plugin" version "1.0"
    }
    ```

* **Gradle 1.x, 2.0**

    ```
    buildscript {
        repositories {
            maven {
                url "https://plugins.gradle.org/m2/"
            }
        }
        dependencies {
            classpath "gradle.plugin.com.gitlab.tguseynov.buildersourcegeneratorlib:builder-source-generator-gradle-plugin:1.0"
        }
    }

    apply plugin: "com.gitlab.tguseynov.builder-source-generator-gradle-plugin"
    ```

# Usage

By default plugin places generated sources in `$projectDir\src\main\generated` folder. Next snippet shows how to
configure another directory:
```
    builder {
        generatedSourcesDir = "\\relative\\to\\project\\directory\\path"
    }
```