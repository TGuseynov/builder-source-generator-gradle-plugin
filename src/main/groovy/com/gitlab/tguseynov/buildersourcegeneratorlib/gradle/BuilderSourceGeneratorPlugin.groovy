package com.gitlab.tguseynov.buildersourcegeneratorlib.gradle

import org.gradle.api.Plugin
import org.gradle.api.Project

class BuilderSourceGeneratorPlugin implements Plugin<Project>
{
	@Override
	void apply(Project target)
	{
		target.plugins.apply("java");

		def extension = new BuilderSourceGeneratorExtension();
		target.extensions.add("builder", extension);

		def cleanTask = target.tasks.getByName("clean");
		def compileJavaTask = target.tasks.getByName("compileJava");

		target.afterEvaluate({
			def fileObject = target.file(extension.generatedSourcesDir);

			cleanTask.doFirst({
				delete fileObject;
			});

			compileJavaTask.doFirst({
				fileObject.exists() || fileObject.mkdirs();
				options.compilerArgs = [
						'-s', fileObject
				];
			});

			target.sourceSets.main.java.srcDirs += fileObject;

			target.javadoc.source = target.sourceSets.main.resources;
		});


		compileJavaTask.dependsOn(cleanTask);
	}
}
